/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.finalprogramexam;

/**
 *
 * @author DELL
 */
public class OrderDate {
    private String dateOrder;
    private String monthOrder;

    public OrderDate(String dateOrder, String monthOrder) {
        this.dateOrder = dateOrder;
        this.monthOrder = monthOrder;
    }

    public String getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(String dateOrder) {
        this.dateOrder = dateOrder;
    }

    public String getMonthOrder() {
        return monthOrder;
    }

    public void setMonthOrder(String monthOrder) {
        this.monthOrder = monthOrder;
    }

    @Override
    public String toString() {
        return dateOrder + " " + monthOrder;
    }
    
}
